#define CATCH_CONFIG_MAIN
#define UNIT_TESTING

#include <catch2/catch_test_macros.hpp>
#include "NetioSender.hpp"
#include "netio3-backend/netio3-backend.h"

using namespace netio3;

TEST_CASE( "Test open connection", "[open_conn_test]" ) {
}

TEST_CASE( "Test close connection", "[close_conn_test]" ) {
}

TEST_CASE( "Test send data", "[send_data_test]" ) {
}

TEST_CASE( "Test send iov", "[send_iov_test]" ) {
}

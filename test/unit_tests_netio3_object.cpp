#define CATCH_CONFIG_MAIN
#define UNIT_TESTING

#include <catch2/catch_test_macros.hpp>
#include "NetioObject.hpp"
#include "netio3-backend/netio3-backend.h"

using namespace netio3;

TEST_CASE( "Test object creation", "[object_test]" ) {
  //"Default" IP and port for testing
  std::string ip = "127.0.0.1";
  unsigned short port = 1337;
  EndPointAddress epaddr(ip, port);
  //Make a backend config, is this enough?
  network_config beConfig{.zero_copy=false};
  beConfig.mode = netio3::TCP;
  //define the network type
  network_type type = netio3::POSIX_SOCKETS;
  //Make the frontend config/eventloop
  netio3::NetioConfig config{type, beConfig, epaddr};
  Eventloop* evloop;
  
  netio3::NetioObject(config, evloop);
  //Assert what?
}

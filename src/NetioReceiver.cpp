// -*- c-basic-offset: 4; -*-
#include "netio3/NetioReceiver.hpp"
#include "netio3-backend/netio3-backend.h"

using namespace netio3;

NetioReceiver::NetioReceiver(NetioConfig& c, Eventloop* e) :
    NetioObject(c, e) {
    DBG(5) << "Entered\n";
}

NetioReceiver::~NetioReceiver() {
    DBG(5) << "Entered "
           << " now have " << m_listeners.size() << " listeners"
           << std::endl;
    for (auto con : m_listeners) {
        DBG(6) << "Closing handle: " << con.second << std::endl;
        m_backend->close_handle(con.second);
    }
    DBG(5) << "Finished\n";
}

void NetioReceiver::set_on_data(CbMessageReceived cb) {
    m_on_msg_received = cb;
}

// Create a listen_handle, pass it to the backend and save for later cleanup
//
// Returns: port on which the backend is listening.
int NetioReceiver::listen(EndPoint& ep) {
    listen_handle handle{
        .local_addr = ep,         // Endpoint (address) to listen on
        .connection_param = connection_parameters{
            .mode=m_config.m_backendConfig.mode,
        },                        // Just use the defaults apart from mode
        .lsocket = NULL           // To be filled in by backend
    };
    m_backend->register_handle(handle);
    m_listeners.insert(std::pair<EndPoint,listen_handle> (ep, handle));
    DBG(6) << " now have " << m_listeners.size() << " listeners"
           << std::endl;

    return handle.local_addr.port();
}


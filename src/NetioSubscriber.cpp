// -*- c-basic-offset: 4; -*-
#include "netio3/NetioSubscriber.hpp"
#include "netio3/NetioSender.hpp"
#include "netio3/NetioReceiver.hpp"
#include "netio3-backend/netio3-backend.h"

using namespace netio3;

NetioSubscriber::NetioSubscriber(NetioConfig& config, Eventloop* evloop)
    : NetioObject(config, evloop) {
    DBG(5) << "Entered backendConfig=" << m_config.m_backendConfig << std::endl;
    NetioConfig sndConf = config;
    sndConf.m_backendConfig.num_buffers = 1;
    sndConf.m_backendConfig.buffersize = 512;
    m_sender.reset(new NetioSender(sndConf, m_evloop));

    m_config.m_backendConfig.on_data_cb = std::bind(&NetioSubscriber::on_data,
                                                    this,
                                                    std::placeholders::_1,
                                                    std::placeholders::_2,
                                                    std::placeholders::_3);
    m_dataReceiver.reset(new NetioReceiver(m_config, m_evloop));

    m_dataReceiver->listen(m_config.m_local_ep);

    network_config ackNetCfg{false, 1, 1024, m_config.m_backendConfig.mode};
    ackNetCfg.on_data_cb = std::bind(&NetioSubscriber::ackSubscribe,
                                     this,
                                     std::placeholders::_1,
                                     std::placeholders::_2,
                                     std::placeholders::_3);
    NetioConfig ackCfg{config.m_backendType, ackNetCfg};
    m_subsReceiver.reset(new NetioReceiver(ackCfg, m_evloop));

    // Listen on system assigned port for acks.
    EndPoint ackEp(m_config.m_local_ep.address(), 0);
    DBG(6) << "Setting subs receiver to listen on " << ackEp << std::endl;
    m_ackPort = m_subsReceiver->listen(ackEp);
    DBG(6) << "m_ackPort=" << m_ackPort << std::endl;
    DBG(5) << "Finished\n";
}

NetioSubscriber::~NetioSubscriber() {
    DBG(5) << "Entered " << m_subscriptions.size()
           << " subscriptions still in list\n";
    std::set<EndPoint> openEps;
    for (auto subs : m_subscriptions) {
        unsubscribe(subs.first, m_endpoints[subs.first]);
        // A bit dodgy deleting the request while it may still be being
        // sent but we're gonna delete the sender next anyway!
        delete subs.second;
        openEps.insert(m_endpoints[subs.first]);
    }
    for (auto ep : openEps) {
        m_sender->close_connection(ep);
    }
    m_dataReceiver.reset();
    m_subsReceiver.reset();
    m_sender.reset();
    DBG(5) << "Finished\n";
}

void NetioSubscriber::ackSubscribe(uint64_t /*tag*/, char* data, int /*size*/) {
    DBG(6) << "Subscription acknowledged\n";
    auto req = reinterpret_cast<SubscriptionRequest*>(data);
    m_on_subscription (req->tag);
}

void NetioSubscriber::on_data(uint64_t tag,
                              char* data, int size) {
    DBG(7) << "Data arrived\n";
    m_on_data_cb(tag, data, size);
}

NetioStatus NetioSubscriber::sendSubscription(SubscriptionRequest* req,
                                              EndPoint& remoteEp) {
    DBG(5) << "Sending " << (req->subscribe ? "" : "un") << "subscription request\n";
    DBG(7) << "SubscriptionRequest=" << *req << std::endl;
    return m_sender->send_data(remoteEp, req->tag,
                               reinterpret_cast<const char*>(req),
                               sizeof(SubscriptionRequest));
}

NetioStatus NetioSubscriber::subscribe(uint64_t tag, EndPoint remoteEp) {
    auto addrlen = m_config.m_local_ep.address().size();
    if (addrlen > SubscriptionRequest::MAXADDR_LEN) {
        std::cerr << "Error endpoint address too long!!\n";
        return NetioStatus::FAILED;
    }
    m_subscriptions[tag] = new SubscriptionRequest {
        .tag=tag,
        .data_port=m_config.m_local_ep.port(),
        .ack_port=m_ackPort,
        .addrlen=addrlen,
        .subscribe=true
    };
    m_endpoints[tag] = remoteEp;
    auto req = m_subscriptions[tag];
    DBG(7) << "SubscriptionRequest=" << *req << std::endl;
    memcpy (req->addr, m_config.m_local_ep.address().data(), addrlen);
    return sendSubscription(req, remoteEp);
}

NetioStatus NetioSubscriber::unsubscribe(uint64_t tag, EndPoint remoteEp) {
    auto reqIter = m_subscriptions.find(tag);
    if (reqIter == m_subscriptions.end()) {
        std::cerr << "Not subscribed to tag " << tag << std::endl;
        return NetioStatus::FAILED;
    }
    reqIter->second->subscribe = false;
    auto status = sendSubscription(reqIter->second, remoteEp);
    // What do we do if the send faild??
    delete reqIter->second;
    m_subscriptions.erase(reqIter);
    return status;
}

void NetioSubscriber::set_on_subscription(CbSubscriptionConfirmed cb) {
    m_on_subscription = cb;
}

void NetioSubscriber::set_on_unsubscription(CbUnsubscriptionConfirmed cb) {
    m_on_unsubscription = cb;
}

void NetioSubscriber::set_on_data(CbMessageReceived cb) {
    // // Forward this on to our NetioReceiver
    // m_dataReceiver->set_on_data(cb);

    m_on_data_cb = cb;
}

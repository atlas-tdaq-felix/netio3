// -*- c-basic-offset: 4; -*-

#include "netio3/NetioBufferedSender.hpp"

using namespace netio3;

NetioBufferedSender::NetioBufferedSender(NetioConfig& config,
                                               Eventloop* evloop)
    : NetioSender(config, evloop),
      m_flush_timer(evloop, std::bind(&NetioBufferedSender::flush_callback, this, std::placeholders::_1, std::placeholders::_2)),
      m_timer_stopped(true) {
    DBG(5) << "Entered" << std::endl;
}

NetioBufferedSender::~NetioBufferedSender() {
    DBG(5) << "Entered" << std::endl;
}

NetioStatus NetioBufferedSender::send_data(const EndPoint& ep, uint64_t tag,
                                           std::vector<iovec>& iov) {
    DBG(7) << "Entered sendig to " << ep << std::endl;
    auto conIter = m_connections.find(ep);
    if (conIter == m_connections.end()) {
        open_connection(ep);
        conIter = m_connections.find(ep);
    }

    if (m_current_buffer[ep] == nullptr) {
        m_current_buffer[ep] = m_backend->get_buffer(m_connections[ep]);
        if (m_current_buffer[ep] == nullptr) {
            return NetioStatus::NO_RESOURCES;
        }

        // Mark this buffer as containing multiple messages ???
        *reinterpret_cast<uint64_t*>(m_current_buffer[ep]->buf) = 0xf5f5f5f5f5f5f5f5;
        m_current_buffer[ep]->pos += sizeof(uint64_t);
    }

    // Work out how much space we need for this publish
    size_t space_required = 0;
    for (auto entry : iov) {
        space_required += entry.iov_len;
    }

    DBG(7) << "buffer size " << m_current_buffer[ep]->size
           << " pos " << m_current_buffer[ep]->pos
           << " diff " << m_current_buffer[ep]->size - m_current_buffer[ep]->pos
           << " space required " << space_required << std::endl;
    if (m_current_buffer[ep]->size - m_current_buffer[ep]->pos < space_required) {
        if (m_current_buffer[ep]->pos > 0) {
            // Try to send this buffer
            NetioStatus status = m_backend->send_buffer(m_connections[ep],
                                                        m_current_buffer[ep]);
            if (status != NetioStatus::OK) {
                return status;
            }
            m_current_buffer[ep] = m_backend->get_buffer(m_connections[ep]);
            if (m_current_buffer[ep] == nullptr) {
                return NetioStatus::NO_RESOURCES;
            }
            // Mark this buffer as containing multiple messages ???
            *reinterpret_cast<uint64_t*>(m_current_buffer[ep]->buf) = 0xf5f5f5f5f5f5f5f5;
            m_current_buffer[ep]->pos += sizeof(uint64_t);
        }
        else {
            // Space required won't fit in an empty buffer so give up
            return NetioStatus::FAILED;
        }
    }

    // OK so we have space, copy the data into the buffer
    DBG(6) << "Adding message of length " << space_required
           << " at position " << m_current_buffer[ep]->pos
           << " in network_buffer at " << m_current_buffer[ep]
           << std::endl;
    for (auto entry : iov) {
        DBG(7) << "Adding chunk of length " << entry.iov_len << std::endl;
        std::memcpy(&m_current_buffer[ep]->buf[m_current_buffer[ep]->pos],
                    entry.iov_base,
                    entry.iov_len);
        m_current_buffer[ep]->pos += entry.iov_len;
    }

    if (m_timer_stopped) {
        m_flush_timer.start_ms(100);
        m_timer_stopped = false;
    }
    return NetioStatus::OK;
}

void NetioBufferedSender::flush_callback(int, void*) {
    DBG(6) << "Entered";
    bool all_clear = true;
    for (auto [ep, buffer] : m_current_buffer) {
        DBG(5) << "Sending buffer to " << ep << std::endl;
        NetioStatus status = NetioSender::send_data(ep,
                                                    0xf5f5f5f5f5f5f5f5,
                                                    buffer->buf,
                                                    buffer->pos);
        if (status == NetioStatus::NO_RESOURCES) {
            all_clear = false;
        }
    }
    if (all_clear) {
        m_flush_timer.stop();
        m_timer_stopped = true;
    }
}

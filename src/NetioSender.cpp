// -*- c-basic-offset: 4; -*-
#include "netio3/NetioSender.hpp"
#include "netio3-backend/netio3-backend.h"

using namespace netio3;

NetioSender::NetioSender(NetioConfig& c, Eventloop* e) :
    NetioObject(c, e) {
    DBG(5) << "Entered backendConfig=" << m_config.m_backendConfig << std::endl;
    DBG(5) << "done\n";
}

NetioSender::~NetioSender() {
    DBG(5) << "entered\n";
    DBG(5) << "done\n";
}

int NetioSender::open_connection(const EndPoint& ep) {
    DBG(5) << "ep=" << ep << std::endl;
    send_handle handle(ep, {
            .mode=m_config.m_backendConfig.mode,
            .buf_size = m_config.m_backendConfig.buffersize,
            .num_buf = m_config.m_backendConfig.num_buffers},
        nullptr);
    m_backend->register_handle(handle);
  
    m_connections.insert(std::pair<EndPoint,send_handle> (ep,handle));
    DBG(5) << "done\n";
    return 0;
}

int NetioSender::close_connection(const EndPoint& ep) {
    DBG(5) << "closing connection to " << ep << std::endl;
    auto conIter = m_connections.find(ep);
    if (conIter == m_connections.end()) {
        std::cerr << "close_connection() EndPoint unknown\n";
        DBG(6) << "m_connections.size()=" << m_connections.size() << std::endl;
        if (debugLevel > 6) {
            std::cout << "Known connections:";
            for (auto con : m_connections) std::cout << " " << con.first;
            std::cout << std::endl;
        }
        return -1;
    }
    m_backend->close_handle(conIter->second);
    m_connections.erase(conIter);
    DBG(5) << "done\n";
    return 0;
}

NetioStatus NetioSender::send_data(const EndPoint& ep, uint64_t tag,
                                   const char* data, size_t length) {
    DBG(6) << "NetioSender::send_data \n" ;
    auto conIter = m_connections.find(ep);
    if (conIter == m_connections.end()) {
        open_connection(ep);
        conIter = m_connections.find(ep);
    }
    return m_backend->send_data(data, length, conIter->second, tag);
    DBG(6) << "done\n";
}

NetioStatus NetioSender::send_data(const EndPoint& ep, uint64_t tag,
                                   std::vector<iovec>& iov) {
    DBG(6) << "NetioSender::send_data \n" ;
    auto conIter = m_connections.find(ep);
    if (conIter == m_connections.end()) {
        open_connection(ep);
        conIter = m_connections.find(ep);
    }
    return m_backend->send_data(iov, conIter->second, tag);
}

void NetioSender::set_on_send_complete(CbSendCompleted cb) {
    // Nasty, messing with config in backend which is a public member for some reason
    m_backend->set_on_send_completed(cb);
}

// -*- c-basic-offset: 4; -*-

#include "netio3/NetioPublisher.hpp"
#include "netio3/NetioSender.hpp"
#include "netio3/NetioBufferedSender.hpp"
#include "netio3/NetioReceiver.hpp"
#include "netio3/SubscriptionRequest.hpp"

using namespace netio3;

NetioPublisher::NetioPublisher(NetioConfig& config, Eventloop* evloop)
    : NetioObject(config, evloop), m_resources_exhausted(false) {
    DBG(5) << "Entered\n";
    network_config rcvNetCfg{false, 1, 1024, config.m_backendConfig.mode};
    rcvNetCfg.on_data_cb = std::bind(&NetioPublisher::subscribe,
                                     this,
                                     std::placeholders::_1,
                                     std::placeholders::_2,
                                     std::placeholders::_3);
    NetioConfig rcvCfg{config.m_backendType, rcvNetCfg};
    m_receiver.reset(new NetioReceiver(rcvCfg, evloop));
    m_receiver->listen(config.m_local_ep);

    if (config.m_buffered) {
        m_sender.reset(new NetioBufferedSender(m_config, m_evloop));
    }
    else {
        m_sender.reset(new NetioSender(m_config, m_evloop));
    }

    // This is for a first attempt at accounting for resource available callback
    m_sender->set_on_send_complete(std::bind(&NetioPublisher::sendComplete,
                                             this,
                                             std::placeholders::_1,
                                             std::placeholders::_2));
    DBG(5) << "Finished\n";
}

NetioPublisher::~NetioPublisher() {
    DBG(5) << "Entered\n";
    for (auto subs: m_subscriptions) {
        DBG(6) << "closing connection to endpoint " << subs.first << std::endl;
        auto ep = subs.first;
        m_sender->close_connection(ep);
        for (auto req : subs.second) {
            DBG(6) << "deleting subscription for tag " << req->tag << std::endl;
            delete req;
        }
    }
    DBG(5) << "Finished\n";
}

void NetioPublisher::sendComplete(uint64_t tag, uint64_t key) {
    DBG(6) << "tag=" << tag << " key=" << key <<std::endl;

    if (m_resources_exhausted) {
        // We must have some resources since a send has completed
        m_resources_exhausted = false;
        // Tell the user
        DBG(6) << "Calling resource available callback" <<std::endl;
        m_on_resource_available();
    }
}

void NetioPublisher::subscribe(uint64_t /*tag*/, char* data, int size) {
    auto req = reinterpret_cast<SubscriptionRequest*>(data);
    DBG(7) << "SubscriptionRequest=" << *req << std::endl;
    std::string addr;
    for (size_t ch=0; ch < req->addrlen; ch++) {
        addr += req->addr[ch];
    }
    EndPoint ep{addr, req->data_port};
    if (req->subscribe) {
        DBG(5) << "Subscription request for tag " << req->tag
               << " from ep=" << addr << ":" << req->data_port << std::endl;

        // Add to our subscription list
        if (m_subscriptions.find(ep) == m_subscriptions.end()) {
            m_subscriptions[ep] = std::vector<SubscriptionRequest*>();
        }
        // Keep a pointer to a copy of the request because we need it to
        // stay at the same address even if the vector gets extended
        m_subscriptions[ep].emplace_back(new SubscriptionRequest{*req});
        m_subscriptionEndpoints[req->tag].emplace_back(ep);

        // then call user's on_subscription callback
        m_on_subscription(req->tag, &ep);

        std::vector<iovec> iov;
        iov.emplace_back(iovec{(void*)m_subscriptions[ep].back(), sizeof(*req)});
        EndPoint ackEp{addr, req->ack_port};
        DBG(5) << "Sending ack for subscription to ep " << ep << " to ack ep " << ackEp << std::endl;
        m_sender->send_data(ackEp, req->tag, iov);
        DBG(6) << "Current subscriptions:  m_subscriptions.size()=" << m_subscriptions.size()
               << ",  m_subscriptions[ep].size()=" << m_subscriptions[ep].size() << std::endl;
    }
    else {
        DBG(5) << "Unsubscribe request for tag " << req->tag
               << " from ep=" << addr << ":" << req->data_port << std::endl;
        // Remove from our subscription list
        for (auto epIter = m_subscriptionEndpoints[req->tag].begin();
             epIter != m_subscriptionEndpoints[req->tag].end(); ++epIter) {
            DBG(5) << "*epIter=" << *epIter << ",  ep=" << ep << std::endl;
            if (*epIter == ep) {
                m_subscriptionEndpoints[req->tag].erase(epIter);
                DBG(5) << "Removed ep from m_subscriptionEndpoints[" << req->tag << "]\n";
                break;
            }
        }
        if (m_subscriptionEndpoints[req->tag].size() == 0) {
            DBG(5) << "No more subscriptions for tag " << req->tag << " removing tag from map\n";
            m_subscriptionEndpoints.erase(req->tag);
        }
        DBG(5) << "Current subscriptions:  m_subscriptions.size()=" << m_subscriptions.size()
               << ",  m_subscriptions[ep].size()=" << m_subscriptions[ep].size() << std::endl;
        // Find the original subscription and remove it from our index
        auto subs = m_subscriptions[ep].begin();
        while (subs != m_subscriptions[ep].end()) {
            SubscriptionRequest* subsReq = *subs;
            if (subsReq->tag == req->tag) {
                DBG(5) << "Found original subscription request, deleting\n";
                // // Acknowledge the unsubscription
                // EndPoint ackEp{addr, req->ack_port};
                // m_sender->send_data(ackEp,
                //                     req->tag,
                //                     reinterpret_cast<const char*>(req),
                //                     sizeof(SubscriptionRequest));
                delete subsReq;
                m_subscriptions[ep].erase(subs);
                subs = m_subscriptions[ep].end();
            }
            else {
                subs++;
            }
        }
        
        // We should close the end point if it is not needed by any of our
        // other subscriptions
        if (m_subscriptions[ep].size() == 0) {
            DBG(5) << "No more subscriptions to ep " << ep << " closing connection\n";
            m_sender->close_connection(ep);
            m_subscriptions.erase(ep);
        }
        // then call user's on_unsubscription callback
        m_on_unsubscription(req->tag, &ep);
        DBG(5) << "Remaining subscriptions:  m_subscriptions.size()=" << m_subscriptions.size() << std::endl;
        int snum = 0;
        for (auto subs : m_subscriptions) {
            DBG(5) << "subs " << ++snum << " of " << m_subscriptions.size() << " subs.first=" << subs.first << std::endl;
        }
        DBG(5) << "Remaining subscriptions:  m_subscriptions.size()=" << m_subscriptions.size() << std::endl;
    }
}

NetioStatus NetioPublisher::publish(uint64_t tag, std::vector<iovec>& iov,
                                    bool retry) {
    NetioStatus status = NetioStatus::OK;
    // Lookup tag in subscription list and send to appropriate end points
    auto subsIter = m_subscriptionEndpoints.find(tag);
    if (subsIter != m_subscriptionEndpoints.end()) {
        if (m_failedSends.find(tag) == m_failedSends.end()) {
            m_failedSends[tag] = std::set<EndPoint>();
        }
        for (auto ep : subsIter->second) {
            DBG(6) << "Publishing to " << ep << " retry=" << retry << std::endl;
            if (!retry || (m_failedSends[tag].find(ep) != m_failedSends[tag].end())) {
                DBG(10) << "Sending\n";
                auto epStat = m_sender->send_data(ep, tag, iov);
                if (epStat != NetioStatus::OK) {
                    // Failed to send Now we need to keep track of which EPs we
                    // did send to and which are still pending
                    m_failedSends[tag].insert(ep);
                    status = NetioStatus::NO_RESOURCES;
                    m_resources_exhausted = true;
                }
            }
            else {
                DBG(10) << "Already sent OK\n";
            }
        }
    }
    else {
        if (retry) {
            // No subscriptions to this tag, someone must have unsubscribed
            auto failIter = m_failedSends.find(tag);
            if (failIter != m_failedSends.end()) {
                // Tidy up failure list
                m_failedSends.erase(failIter);
            }
        }
        DBG(6) << "No subscriptions for tag " << std::hex << tag << std::dec << std::endl;
        status = NetioStatus::NO_SUBSCRIPTIONS;
    }
    return status;
}

void NetioPublisher::set_on_subscription(CbSubscriptionReceived cb) {
    m_on_subscription = cb;
}
void NetioPublisher::set_on_unsubscription(CbUnsubscriptionReceived cb) {
    m_on_unsubscription = cb;
}
void NetioPublisher::set_on_resource_available(CbResourceAvailable cb) {
    m_on_resource_available = cb;
}

// local variables:
// c-basic-offset: 4
// end:

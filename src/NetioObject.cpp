// -*- c-basic-offset: 4; -*-
#include "netio3/NetioObject.hpp"
#include "netio3-backend/netio3-backend.h"

using namespace netio3;

NetioObject::NetioObject(NetioConfig& config, Eventloop* evloop) :
    m_config(config), m_evloop(evloop) {
    m_backend = NetworkBackend::create(config.m_backendType,
                                       config.m_backendConfig, evloop);
}

void NetioObject::set_on_connection_established(CbConnectionEstablished cb) {
    m_on_connection_established = cb;
}
void NetioObject::set_on_connection_closed(CbConnectionClosed cb) {
    m_on_connection_closed = cb;
}
void NetioObject::set_on_connection_refused(CbConnectionRefused cb) {
    m_on_connection_refused = cb;
}
void NetioObject::set_on_user_timer(CbUserTimer cb) {
    m_on_user_timer = cb;
}

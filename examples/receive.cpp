#include "netio3/NetioReceiver.hpp"

#include "docopt/docopt.h"

#include <csignal>

using namespace netio3;

static const char USAGE[] =
R"(receive - Receiving test app.

    Usage:
      receive [options]

    Options:
      -h, --help                         Show this screen.
          --version                      Show version.
      -b, --buffers=BUFFERS              Number of send buffers [default: 16]
      -i, --ip=IP                        Specify ip address [default: 127.0.0.1]
      -p, --port=PORT                    Specify port [default: 1337]
      -m, --mode=NETWORK_MODE            Specify network mode [default: TCP]
      -t, --type=NETWORK_TYPE            Specify network type [default: POSIX_SOCKETS]
      -s, --size=BUFFER_SIZE             Size of message buffers in 32bit words [default: 256]
      -r, --report-interval=MESSAGES     Number of messages between stats reports [default: 1000]
    Network Mode: TCP, UDP, RDMA, SHMEM
    Network Type: NONE, POSIX_SOCKETS, LIBFABRIC, UCX
)";


Eventloop evloop;

void sighandler(int) {
  std::cout << "Stopping event loop\n";
  evloop.stop();
}

void on_data(uint64_t tag, char* data, int size) {
  std::cout << "Received message size=" << size << ", data " << std::hex;
  auto dp = reinterpret_cast<uint32_t*>(data);
  for (int i=0; i<12; i++) {
    std::cout << " " << dp[i];
  }
  std::cout << "..." << std::dec << std::endl;
}

int main(int argc, char* argv[]) {
  std::map<std::string, docopt::value> args = docopt::docopt(
    USAGE, { argv + 1, argv + argc });

  network_config beConfig{
    .zero_copy=false,
    // .usr_ctx=&userContext,
    .on_data_cb = on_data
  };
  network_type networkType;

  std::string ip;
  unsigned short port;
  try {
    ip = args["--ip"].asString();
    port = args["--port"].asLong();
    beConfig.mode = network_mode_from_string(args["--mode"].asString());
    networkType = network_type_from_string(args["--type"].asString());
    beConfig.buffersize = args["--size"].asLong();
    beConfig.num_buffers = args["--buffers"].asLong();
  } catch (std::invalid_argument const& error) {
    std::cerr << "Argument or option of wrong type" << std::endl
              << error.what() << std::endl;
    std::cout << std::endl << USAGE << std::endl;
    exit(-1);
  }
  NetioConfig config{networkType, beConfig};
  NetioReceiver rcvr(config, &evloop);
  EndPointAddress ipaddr(ip, port);
  rcvr.listen(ipaddr);

  struct sigaction sa;
  sa.sa_handler=&sighandler;
  sigaction(SIGINT, &sa, 0);
  evloop.evloop_run();

  return 0;
}

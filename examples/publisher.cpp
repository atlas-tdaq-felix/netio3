// -*- c-basic-offset: 4; -*-
#include "netio3/NetioPublisher.hpp"
#include "netio3-backend/MessageHeader.hpp"

#include "docopt/docopt.h"

#include <csignal>
#include <sstream>
#include <thread>
#include <chrono>

using namespace std::literals::chrono_literals;

using namespace netio3;

static const char USAGE[] =
    R"(publisher - publishing test app.

    Usage:
      publish [options] <tag> ...

    Options:
      -h, --help                         Show this screen.
          --version                      Show version.
      -b, --buffers=BUFFERS              Number of send buffers [default: 16]
      -B, --buffered                     Use buffered mode
      -i, --ip=IP                        Specify ip address [default: 127.0.0.1]
      -p, --port=PORT                    Specify port [default: 1337]
      -m, --mode=NETWORK_MODE            Specify network mode [default: TCP]
      -t, --type=NETWORK_TYPE            Specify network type [default: POSIX_SOCKETS]
      -s, --size=BUFFER_SIZE             Size of message buffers in 32bit words [default: 256]
      -r, --report-interval=MESSAGES     Number of messages between stats reports [default: 1000]
      -d, --delay=TIMER_INTERVAL         Delay in seconds between publish attempts [default: 1s]
      -g, --grouping=GROUP_SIZE          Number of packets to attempt to publish in one group [default: 1000]
      --debug-level=LEVEL            debug level for netio3 [default: 0]
    Network Mode: TCP, UDP, RDMA, SHMEM
    Network Type: NONE, POSIX_SOCKETS, LIBFABRIC, UCX
)";


Eventloop evloop;

class Publisher {
    NetioPublisher m_publisher;
    std::vector<uint64_t> m_tags;
    std::vector<iovec> m_iov;
    std::vector<uint32_t> m_buffer;
    uint32_t m_groupSize;
    uint32_t m_sentInGroup;
    uint64_t m_nPublished;
    uint32_t m_reportInterval;
    uint64_t m_timer_interval;
    EvTimer m_timer;
    bool m_pending;
public:
    Publisher(NetioConfig networkConfig,
              network_config beConfig,
              std::vector<uint64_t> tags,
              uint32_t dataSize,
              uint32_t groupSize,
              uint32_t reportInterval,
              uint32_t timer_interval) 
        : m_publisher(networkConfig, &evloop),
          m_tags(tags), m_groupSize(groupSize),
          m_sentInGroup(0), m_nPublished(0),
          m_reportInterval(reportInterval),
          m_timer_interval(timer_interval),
          m_timer(&evloop, std::bind(&Publisher::timerCallback, this,  std::placeholders::_1,  std::placeholders::_2)),
          m_pending(false) {
        m_publisher.set_on_subscription(
            std::bind(&Publisher::subscriptionCallback,this,  std::placeholders::_1,  std::placeholders::_2));
        m_publisher.set_on_resource_available(std::bind(&Publisher::resourcesCallback,this));

        m_buffer.resize(beConfig.buffersize);
        m_iov.resize(1);
        m_iov[0].iov_base = m_buffer.data();
        m_iov[0].iov_len = dataSize * 4;
        m_timer.start_us(m_timer_interval);
    }

    void resourcesCallback() {
        DBG(6) << "entered\n";
        publishGroup();
        if (!m_pending) {
            m_sentInGroup = 0;
        }
        else {
            DBG(6) << "publish pending, waiting for resources to become available\n";
        }
    }

    void publishGroup() {
        DBG(6) << "Publishing for tag " << std::hex << m_tags[0] << std::dec
               << " pending=" << m_pending << std::endl;
        auto status = NetioStatus::OK;
        while (status == NetioStatus::OK && m_sentInGroup < m_groupSize) {
            status = m_publisher.publish(m_tags[0], m_iov, m_pending);
            if (status == NetioStatus::OK) {
                m_sentInGroup++;
                m_pending = false;
                m_nPublished++;
                if (m_nPublished%m_reportInterval == 0) {
                    std::cout << "Published " << m_nPublished << " packets\n";
                }
            }
            else if (status != NetioStatus::NO_SUBSCRIPTIONS) {
                m_pending = true;
            }
        }
        DBG(6) << "Finished sent " << m_sentInGroup
               << " of " << m_groupSize 
               << " packets in group so far, pending=" << m_pending
               << std::endl;
    }

    void timerCallback(int, void* /*data*/) {
        DBG(6) << "entered\n";
        publishGroup();
        if (!m_pending) {
            m_sentInGroup = 0;
        }
        else {
            DBG(6) << "publish pending, waiting for resources to become available\n";
        }
    }

    void subscriptionCallback(uint64_t tag, EndPoint* ep) {
        std::cout << "User subscription callback tag=" << std::hex << tag << std::dec << std::endl;
        std::this_thread::sleep_for(500ms);
    }
};




void sighandler(int) {
    std::cout << "Stopping event loop\n";
    evloop.stop();
}


int main(int argc, char* argv[]) {
    std::map<std::string, docopt::value> args = docopt::docopt(
        USAGE, { argv + 1, argv + argc });
    network_config beConfig{
        .zero_copy=false,
        // .usr_ctx=&userContext,
        //.on_data_cb = on_data
    };
    network_type networkType;
    std::vector<uint64_t> tags;
    std::string timerDelay;
    uint32_t groupSize;
    uint32_t reportInterval;
    uint32_t dataSize;
    std::string ip;
    unsigned short port;
    bool buffered;
    try {
        ip = args["--ip"].asString();
        port = args["--port"].asLong();
        for (auto tag : args["<tag>"].asStringList()) {
            tags.push_back(std::stoi(tag, nullptr, 16));
        }
        buffered = args["--buffered"].asBool();
        beConfig.mode = network_mode_from_string(args["--mode"].asString());
        networkType = network_type_from_string(args["--type"].asString());
        dataSize =  args["--size"].asLong();
        beConfig.buffersize = dataSize*4 + sizeof(netio3::MessageHeader);
        beConfig.num_buffers = args["--buffers"].asLong();
        timerDelay = args["--delay"].asString();
        groupSize = args["--grouping"].asLong();
        reportInterval = args["--report-interval"].asLong();
        netio3::debugLevel = args["--debug-level"].asLong();
    } catch (std::invalid_argument const& error) {
        std::cerr << "Argument or option of wrong type" << std::endl
                  << error.what() << std::endl;
        std::cout << std::endl << USAGE << std::endl;
        exit(-1);
    }


    EndPointAddress epaddr(ip, port);
    NetioConfig config{networkType, beConfig, epaddr, buffered};

    size_t upos;
    uint64_t timer_interval = std::stoul(timerDelay, &upos);
    uint32_t scale = 1000000;
    if (timerDelay.size() > upos ) {
        if (timerDelay[upos] == 'm') {
            scale = 1000;
        }
        if (timerDelay[upos] == 'u') {
            scale = 1;
        }
    }
    timer_interval = timer_interval * scale;
    Publisher cbd(config, beConfig, tags,
                  dataSize, groupSize, reportInterval,
                  timer_interval) ;

    struct sigaction sa;
    sa.sa_handler=&sighandler;
    sigaction(SIGINT, &sa, 0);

    evloop.evloop_run();

    return 0;
}

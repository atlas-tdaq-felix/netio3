// -*- c-basic-offset: 4; -*-
#include "netio3/NetioSubscriber.hpp"

#include "docopt/docopt.h"

#include <csignal>
#include <thread>
#include <chrono>

using namespace std::literals::chrono_literals;

using namespace netio3;

static const char USAGE[] =
    R"(subscriber - subscribing test app.

    Usage:
      subscribe [options] <local-endpoint> <remote-endpoint> <tag>...

    Options:
      -h, --help                         Show this screen.
          --version                      Show version.
      -m, --mode=NETWORK_MODE            Specify network mode [default: TCP]
      -t, --type=NETWORK_TYPE            Specify network type [default: POSIX_SOCKETS]
      -b, --buffers=BUFFERS              Number of send buffers [default: 16]
      -s, --size=BUFFER_SIZE             Size of message buffers in 32bit words [default: 256]
      -r, --report-interval=MESSAGES     Number of messages between stats reports [default: 1000]
      --debug-level=LEVEL            debug level for netio3 [default: 0]

    Network Mode: TCP, UDP, RDMA, SHMEM
    Network Type: NONE, POSIX_SOCKETS, LIBFABRIC, UCX
)";


Eventloop evloop;
NetioSubscriber* subscriber;
EndPointAddress remoteEp;
std::set<uint64_t> tags;
uint64_t packetsReceived = 0;
uint64_t reportInterval = 1000;

void sighandler(int) {
    for (auto tag : tags) {
        std::cout << "Unsubscribing from tag " << tag << std::endl;
        subscriber->unsubscribe(tag, remoteEp);
        tags.erase(tag);
    }
    if (tags.size() == 0) {
        std::cout << "No more subscriptions, stopping event loop\n";
        evloop.stop();
    }
}

void subscriptionCallback(uint64_t tag) {
    std::cout << "User subscription confirmed callback tag=" << std::hex << tag << std::dec << std::endl;
}
void unSubscriptionCallback(uint64_t tag) {
    std::cout << "User unsubscription confirmation callback tag=" << std::hex << tag << std::dec << std::endl;
    // tags.erase(tag);
    // if (tags.size() == 0) {
    //   std::cout << "No more subscriptions, stopping event loop\n";
    //   evloop.stop();
    // }
}

//void dataReceivedCallback(uint64_t tag, std::vector<iovec>& iov) {
void dataReceivedCallback(uint64_t tag, char* data, size_t length) {
    DBG(7) << "User data received callback tag=" << tag << std::endl;
    packetsReceived++;
    if (packetsReceived%reportInterval == 0) {
        std::cout << packetsReceived << " data packets received\n";
    }
}

typedef struct {
    std::vector<uint64_t> tags;
    EndPointAddress remoteEp;
    EvSignal esig;
} SigData;

void initCallback(int, void* data) {
    auto sd = reinterpret_cast<SigData*>(data);
    auto tag = sd->tags.back();
    std::cout << "subscriber: Subscribing to tag " << std::hex << tag << std::dec
              << " on addr " << sd->remoteEp << std::endl;
    if (subscriber->subscribe(tag, sd->remoteEp) == 0) {
        sd->tags.pop_back();
    }
    if (sd->tags.size() != 0) {
        sd->esig.fire();
    }
}

int main(int argc, char* argv[]) {
    std::map<std::string, docopt::value> args = docopt::docopt(
        USAGE, { argv + 1, argv + argc });
    network_config beConfig{
        .zero_copy=false
    };
    network_type networkType;

    std::string localAddr;
    std::string remoteAddr;
    try {
        localAddr = args["<local-endpoint>"].asString();
        remoteAddr = args["<remote-endpoint>"].asString();
        for (auto tag : args["<tag>"].asStringList()) {
            tags.insert(std::stoi(tag, nullptr, 16));
        }
        beConfig.mode = network_mode_from_string(args["--mode"].asString());
        networkType = network_type_from_string(args["--type"].asString());
        beConfig.buffersize = args["--size"].asLong();
        beConfig.num_buffers = args["--buffers"].asLong();
        reportInterval = args["--report-interval"].asLong();
        netio3::debugLevel = args["--debug-level"].asLong();
    } catch (std::invalid_argument const& error) {
        std::cerr << "Argument or option of wrong type" << std::endl
                  << error.what() << std::endl;
        std::cout << std::endl << USAGE << std::endl;
        exit(-1);
    }
    EndPointAddress localEp(localAddr);
    remoteEp = EndPointAddress(remoteAddr);
    std::cout << "subscriber: local address " << localEp
              << " n be buffers=" << beConfig.num_buffers
              << std::endl;
    NetioConfig config{networkType, beConfig, localEp};
    subscriber = new NetioSubscriber(config, &evloop);
    subscriber->set_on_subscription(&subscriptionCallback);
    subscriber->set_on_unsubscription(&unSubscriptionCallback);
    subscriber->set_on_data(&dataReceivedCallback);
    SigData sd{.tags=std::vector<uint64_t>(),
        .remoteEp=remoteEp,
        .esig={&sd, initCallback, &evloop, true, false}
    };
    for (auto tag : tags) {
        sd.tags.push_back(tag);
    }
    sd.esig.fire();

    struct sigaction sa;
    sa.sa_handler=&sighandler;
    sigaction(SIGINT, &sa, 0);

    std::thread eloopThread ([] () {evloop.evloop_run();});

    eloopThread.join();

    std::this_thread::sleep_for(1s);
    std::cout << "deleteing subscriber\n";
    delete subscriber;
    std::cout << "sleeping\n";
    std::this_thread::sleep_for(1s);

    return 0;
}

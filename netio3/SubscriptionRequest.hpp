// -*- c-basic-offset: 4; -*-
#pragma once

#include <iostream>

namespace netio3
{
    struct SubscriptionRequest {
        enum {MAXADDR_LEN=256};
        uint64_t tag;
        uint16_t data_port;
        uint16_t ack_port;
        size_t addrlen;
        char addr[MAXADDR_LEN];
        bool subscribe;
        friend std::ostream& operator << (std::ostream&, const SubscriptionRequest& req);
    };
    inline std::ostream& operator << (std::ostream& stream, const SubscriptionRequest& req) {
        return stream << "{tag 0x" << std::hex << req.tag << std::dec
                      << ", data_port " << req.data_port
                      << ",ack_port " << req.ack_port
                      << ", subscribe " << req.subscribe
                      << "}";
    }
} // namespace netio3

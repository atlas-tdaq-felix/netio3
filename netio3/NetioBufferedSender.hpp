// -*- c-basic-offset: 4; -*-
#pragma once

#include "netio3/NetioConfig.hpp"
#include "netio3/NetioSender.hpp"

#include <cstdint>
#include <functional>
#include <memory>
#include <map>

namespace netio3
{
    class NetioBufferedSender : public NetioSender {
    public:
        NetioBufferedSender(NetioConfig& c, Eventloop* e = nullptr);
        virtual ~NetioBufferedSender();
        virtual NetioStatus send_data(const EndPoint& ep, uint64_t tag,
                                      std::vector<iovec>& iov) override;
    private:
        void flush_callback(int, void*);

        //? NetioBufferedSender m_sender; //??
        std::map<EndPoint, network_buffer*> m_current_buffer;

        EvTimer m_flush_timer;
        bool m_timer_stopped;

    };
} // namespace netio3

// -*- c-basic-offset: 4; -*-
#pragma once

#include "netio3/NetioConfig.hpp"
#include "netio3/NetioObject.hpp"

#include <cstdint>
#include <functional>
#include <vector>

namespace netio3
{
    // sender callbacks
    typedef std::function<void(uint64_t tag, uint64_t key)> CbSendCompleted;

    /// Netio3 sender class. Sends blocks of data to a Netio3 peer
    class NetioSender : public NetioObject
    {
    protected:
        /// Callback function to be called when send completes
        CbSendCompleted m_on_send_complete = [](auto&&...) {};

        std::map<EndPoint, send_handle> m_connections;

        /// MessageQueue not yet implemented
        class MessageQueue {
            // To be defined!!
            uint64_t m_maxLength;
        };
        MessageQueue m_msg_queue;
    public:
        NetioSender(NetioConfig& c, Eventloop* e = nullptr);
        ~NetioSender();
            
        int open_connection(const EndPoint& ep);
        int close_connection(const EndPoint& ep);

        virtual NetioStatus send_data(const EndPoint& ep, uint64_t tag, const char* data, size_t length);
        virtual NetioStatus send_data(const EndPoint& ep, uint64_t tag, std::vector<iovec>& iov);

        //TBD
        int push_data_to_send_queue(uint64_t tag, std::vector<iovec>& iov);
        int send_data_from_queue(uint64_t tag);

        void set_on_send_complete(CbSendCompleted cb);
    };
} // namespace netio3

// -*- c-basic-offset: 4; -*-
#pragma once

#include "netio3-backend/netio3-backend.h"

#include <cstdint>
#include <functional>
#include <memory>
#include <string>
#include <vector>

namespace netio3
{
    typedef EndPointAddress EndPoint; // Possibly a new class instead

    struct NetioConfig {
        // To be defined!!
        network_type m_backendType;
        network_config m_backendConfig;
        EndPoint m_local_ep;
        bool m_buffered;
    };
}

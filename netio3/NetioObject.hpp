// -*- c-basic-offset: 4; -*-
#pragma once

#include "netio3/NetioConfig.hpp"

#include <cstdint>
#include <functional>
#include <memory>

namespace netio3 {
    class Eventloop;
    class NetworkBackend;
    // common callbacks
    typedef std::function<void(EndPoint* ep)> CbConnectionEstablished;
    typedef std::function<void(EndPoint* ep)> CbConnectionClosed;
    typedef std::function<void(EndPoint* ep)> CbConnectionRefused;
    typedef std::function<void ()> CbUserTimer;

    /// Abstract base class for the Netio3 communication classes
    class NetioObject {
    protected:
        NetioConfig m_config;
        Eventloop* m_evloop = nullptr;
        std::unique_ptr<NetworkBackend> m_backend;

        CbConnectionEstablished m_on_connection_established = [](auto&&...) {};
        CbConnectionClosed m_on_connection_closed = [](auto&&...) {};
        CbConnectionRefused m_on_connection_refused = [](auto&&...) {};
        CbUserTimer m_on_user_timer  = [](auto&&...) {};

        NetioObject(NetioConfig& c, Eventloop* e = nullptr);
    public:
        void set_on_connection_established(CbConnectionEstablished cb);
        void set_on_connection_closed(CbConnectionClosed cb);
        void set_on_connection_refused(CbConnectionRefused cb);
        void set_on_user_timer(CbUserTimer cb);
    };
} // namespace netio3

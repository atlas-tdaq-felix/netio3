// -*- c-basic-offset: 4; -*-
#pragma once

#include "netio3/NetioConfig.hpp"
#include "netio3/NetioObject.hpp"

#include <cstdint>
#include <functional>
#include <memory>
#include <map>

namespace netio3
{
    class NetioSender;
    class NetioReceiver;
    class SubscriptionRequest;

    // publisher callbacks
    typedef std::function<void(uint64_t tag, EndPoint* ep)> CbSubscriptionReceived;
    typedef std::function<void(uint64_t tag, EndPoint* ep)> CbUnsubscriptionReceived;
    typedef std::function<void()> CbResourceAvailable;

    // publisher class
    class NetioPublisher : NetioObject
    {
    protected:
        CbSubscriptionReceived m_on_subscription = [](auto&&...) {};
        CbUnsubscriptionReceived m_on_unsubscription = [](auto&&...) {};
        CbResourceAvailable m_on_resource_available = [](auto&&...) {};

        std::map<uint64_t, std::vector<EndPoint>> m_subscriptionEndpoints;
        std::map<EndPoint, std::vector<SubscriptionRequest*>> m_subscriptions;
        std::unique_ptr<NetioSender> m_sender;
        std::unique_ptr<NetioReceiver> m_receiver;
        std::map<uint64_t, std::set<EndPoint>> m_failedSends;
        bool m_resources_exhausted;

        // NetworkBackend on_data callback to handle subscription request
        void subscribe(uint64_t tag, char* data, int size);
        void sendComplete(uint64_t tag, uint64_t key);
    public:
        NetioPublisher(NetioConfig& c, Eventloop* e = nullptr);
        virtual ~NetioPublisher();

        virtual NetioStatus publish(uint64_t tag, std::vector<iovec>& iov, bool retry);

        void set_on_subscription(CbSubscriptionReceived cb);
        void set_on_unsubscription(CbUnsubscriptionReceived cb);
        void set_on_resource_available(CbResourceAvailable cb);
    };
} // namespace netio3

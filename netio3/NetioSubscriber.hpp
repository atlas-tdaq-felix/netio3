// -*- c-basic-offset: 4; -*-
#pragma once

#include "netio3/NetioConfig.hpp"
#include "netio3/NetioObject.hpp"
#include "netio3/SubscriptionRequest.hpp"

#include <cstdint>
#include <functional>
#include <map>

namespace netio3
{
    class NetioSender;
    class NetioReceiver;

    // subscriber callbacks
    typedef std::function<void(uint64_t tag)> CbSubscriptionConfirmed;
    typedef std::function<void(uint64_t tag)> CbUnsubscriptionConfirmed;
    //typedef std::function<void(uint64_t tag, std::vector<iovec>& iov)> CbMessageReceived;
    typedef std::function<void(uint64_t tag, char*data, size_t length)> CbMessageReceived;

    // subscriber class
    class NetioSubscriber : NetioObject
    {
    private:
        CbSubscriptionConfirmed m_on_subscription = [](auto&&...) {};
        CbUnsubscriptionConfirmed m_on_unsubscription = [](auto&&...) {};
        CbMessageReceived m_on_data_cb= [](auto&&...) {};

        std::unique_ptr<NetioSender> m_sender;  // For sending subscription requests
        std::unique_ptr<NetioReceiver> m_subsReceiver; // For receiving subscription acknowledgements
        std::unique_ptr<NetioReceiver> m_dataReceiver; // For receiving subscribed data

        uint16_t m_ackPort;
        std::map<uint64_t, SubscriptionRequest*> m_subscriptions;
        std::map<uint64_t, EndPoint> m_endpoints;

        NetioStatus sendSubscription(SubscriptionRequest*, EndPoint& remoteEp);
        // Backend callbacks
        void ackSubscribe(uint64_t tag, char* data, int size);
        void on_data(uint64_t tag, char* data, int size);
    public:
        NetioSubscriber(NetioConfig& c, Eventloop* e = nullptr);
        ~NetioSubscriber();

        NetioStatus subscribe(uint64_t tag, EndPoint remote_ep);
        NetioStatus unsubscribe(uint64_t tag, EndPoint remote_ep);
            
        void set_on_subscription(CbSubscriptionConfirmed cb);
        void set_on_unsubscription(CbUnsubscriptionConfirmed cb);
        void set_on_data(CbMessageReceived cb);
    };
} // namespace netio3

// -*- c-basic-offset: 4; -*-
#pragma once

#include "netio3/NetioConfig.hpp"
#include "netio3/NetioObject.hpp"

#include <cstdint>
#include <functional>
#include <vector>

namespace netio3
{
    typedef std::function<void(uint64_t tag, char*data, size_t length)> CbMessageReceived;

    // receiver class
    class NetioReceiver : NetioObject
    {
    private:
        CbMessageReceived m_on_msg_received = [](auto&&...) {};
        std::map<EndPoint, listen_handle> m_listeners;
    public:
        NetioReceiver(NetioConfig& c, Eventloop* e = nullptr);
        ~NetioReceiver();
        void set_on_data(CbMessageReceived cb);
        int listen(EndPoint& pep);
    };
} // namespace netio3
